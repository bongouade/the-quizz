import 'package:flutter/material.dart';

import 'package:rflutter_alert/rflutter_alert.dart';

class QuizzBuilder {
  static Widget close() {
    return Icon(
      Icons.close,
      color: Colors.red,
    );
  }

  static Widget check() {
    return Icon(
      Icons.check,
      color: Colors.green,
    );
  }

  static Widget buildQuestion(String questiontext) {
    return Padding(
      padding: EdgeInsets.all(15.0),
      child: Center(
          child: Text(
        questiontext,
        textAlign: TextAlign.center,
        style: TextStyle(
          color: Colors.white,
          fontSize: 25.0,
        ),
      )),
    );
  }

  static Widget buildButton({
    @required String name,
    @required void onTap(),
    @required MaterialColor color,
  }) {
    return Padding(
      padding: EdgeInsets.all(10.0),
      child: FlatButton(
        textColor: color,
        color: Colors.white,
        onPressed: onTap,
        child: Text(name,
            style: TextStyle(
              fontSize: 20.0,
            )),
      ),
    );
  }

  static Widget buttonDial({String name, void onTap()}) {
    return DialogButton(
      child: Text(name),
      onPressed: onTap,
    );
  }
}
